package ru.lar.shop.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ru.lar.shop.model.Product;

@Repository
public interface ProductDAO extends JpaRepository<Product, Long> {

}
