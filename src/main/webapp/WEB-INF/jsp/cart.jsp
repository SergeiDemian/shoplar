<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ page contentType="text/html;charset=UTF-8" language="java"
	pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
<title>Корзина</title>
</head>
<body>
	<%@ include file="templates/menubar.jsp"%>
	<h1 align="center" class="form-signin-heading">Оформление заказа</h1>
	<div class="container">
		<c:choose>
			<c:when test="${not empty order}">
				<table class="table table-hover table-sm">
					<tr class="table-active" align="center">
						<th>Наименование продукта</th>
						<th>Цена</th>
						<th>Кол-во</th>
						<th>Сумма</th>
					</tr>
					<c:forEach items="${order.mapShopOrders}" var="shopOrder">
						<tr align="center">
							<td>${shopOrder.key.product.productName}</td>
							<td>${shopOrder.key.product.price}</td>
							<td>${shopOrder.key.count}</td>
							<td>${shopOrder.value}</td>
						</tr>
					</c:forEach>
					<tr>
						<td colspan="3" align="right">Итого, в том числе
							НДС-${order.sumOfNDS}:</td>
						<td align="center">${order.sumPriceWithNDS}</td>
					</tr>
					<tr>
						<td colspan="3" align="right">Доставка:</td>
						<td align="center">${order.dileviry}</td>
					</tr>
					<tr>
						<td colspan="3" align="right">Итого к оплате:</td>
						<td align="center">${order.fullSumm}</td>
					</tr>
				</table>
				<form:form action="/order" method="post" modelAttribute="order"
					name="order">
					<c:set var="shopOrderList" value="${order}" scope="session" />
					<button type="submit" form="order" onclick="shopOrderList"
						class="btn btn-primary btn-lg btn-block">Оформить заказ</button>
				</form:form>
			</c:when>
			<c:otherwise>
				<c:out value="Корзина пуста, приступите к выбору товаров."></c:out>
			</c:otherwise>
		</c:choose>
	</div>
	<br />
</body>
</html>