package ru.lar.shop.controllers;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import lombok.NoArgsConstructor;
import ru.lar.shop.common.Common;
import ru.lar.shop.dao.CartDAO;
import ru.lar.shop.dao.OrderDAO;
import ru.lar.shop.dao.ProductDAO;
import ru.lar.shop.model.Product;
import ru.lar.shop.services.CartService;
import ru.lar.shop.services.OrderService;
import ru.lar.shop.services.ProductService;

@Controller
@Scope(scopeName = "session")
@NoArgsConstructor
public class ShopController {

	private CartDAO cardDAO;

	private OrderDAO orderDAO;
	private ProductDAO productDAO;
	List<Product> products = new ArrayList<>();

	@Autowired
	public ShopController(ProductDAO productDAO, OrderDAO orderDAO, CartDAO cartDAO) {
		super();
		this.productDAO = productDAO;
		this.orderDAO = orderDAO;
		this.cardDAO = cartDAO;
	}

	@RequestMapping(value = "/index", method = RequestMethod.POST)
	public ModelAndView addOrder(@ModelAttribute("order") Product product) {
		products.add(product);
		return new ModelAndView("redirect:index");
	}

	@Transactional
	@RequestMapping(value = "/order", method = RequestMethod.POST)
	public ModelAndView createOrder(HttpSession httpSession) {
		new OrderService(orderDAO, cardDAO).createOrder((Common) httpSession.getAttribute("shopOrderList"));
		products.removeAll(products);
		return new ModelAndView("redirect:done");
	}

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView firstPage(ModelAndView modelAndView) {
		modelAndView.addObject("products", new ProductService(productDAO).getListProduct());
		modelAndView.addObject("order", new Product());
		modelAndView.setViewName("index");
		return modelAndView;
	}

	@RequestMapping(value = "/cart", method = RequestMethod.GET)
	public ModelAndView getCart(ModelAndView modelAndView) {
		modelAndView.addObject("order", new CartService().getCart(products));
		return modelAndView;
	}

	@RequestMapping(value = "/done", method = RequestMethod.GET)
	public ModelAndView getDone(ModelAndView modelAndView) {
		modelAndView.setViewName("doneorder");
		return modelAndView;
	}
}
