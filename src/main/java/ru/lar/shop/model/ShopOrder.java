package ru.lar.shop.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShopOrder {

	@OneToMany(mappedBy = "shopOrder")
	private Set<Cart> carts;

	@Column
	private Date createDate;

	@Column
	private double dileviry;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column
	private int nds;

	public ShopOrder(int nds, double dileviry, Date createDate) {
		super();
		this.nds = nds;
		this.dileviry = dileviry;
		this.createDate = createDate;
	}

}
