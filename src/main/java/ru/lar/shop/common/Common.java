package ru.lar.shop.common;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.context.annotation.Bean;

import lombok.Data;
import ru.lar.shop.model.Cart;
import ru.lar.shop.model.ShopOrder;

@Data
public class Common {

	private double dileviry = -1;

	private double dileviryWithNDS;

	private double fullSumm;

	private long id;

	private Map<Cart, Double> mapShopOrders = new LinkedHashMap<>();

	private int NDS = 20;

	private double sumAllPriceWithoutNDSAndDileviry;

	private double sumOfNDS;

	private double sumPriceWithNDS;

	@Bean
	public Common getCommonforOrder(ShopOrder shopOrder) {

		NDS = shopOrder.getNds();

		dileviry = shopOrder.getDileviry();

		id = shopOrder.getId();

		this.getFullPrice(shopOrder.getCarts().stream().collect(Collectors.toList()));

		return this;
	}

	@Bean
	public Common getFullPrice(List<Cart> carts) {

		carts.forEach(
				x -> mapShopOrders.put(x, (double) Math.round(((double) (x.getCount() * x.getProduct().getPrice())))));

		sumAllPriceWithoutNDSAndDileviry = mapShopOrders.values().stream().reduce(0.0, Double::sum);
		sumOfNDS = sumAllPriceWithoutNDSAndDileviry * NDS / 100.0;

		sumPriceWithNDS = Math.round((sumAllPriceWithoutNDSAndDileviry + sumOfNDS) * 100) / 100;

		if (dileviry < 0.0)
			dileviry = Math.round((sumAllPriceWithoutNDSAndDileviry * 5.0) * 100) / 10000.0;

		dileviryWithNDS = Math.round((dileviry + (dileviry * NDS) / 100.0) * 100) / 100.0;

		fullSumm = dileviryWithNDS + sumPriceWithNDS;

		return this;
	}
}
